INSTALLATION
A new Ceph cluster is deployed by bootstrapping a cluster on a single node, and then adding additional nodes and daemons via the CLI or GUI dashboard.

The following example installs a basic three-node cluster. Each node will be identified by its prompt. For example, “[monitor 1]” identifies the first monitor, “[monitor 2]” identifies the second monitor, and “[monitor 3]” identifies the third monitor. This information is provided in order to make clear which commands should be issued on which systems.

“[any node]” identifies any Ceph node, and in the context of this installation guide means that the associated command can be run on any node.