BUCKET NOTIFICATIONS

Bucket notifications provide a mechanism for sending information out of the radosgw when certain events are happening on the bucket. Currently, notifications could be sent to: HTTP, AMQP0.9.1 and Kafka endpoints.

Note, that if the events should be stored in Ceph, in addition, or instead of being pushed to an endpoint, the PubSub Module should be used instead of the bucket notification mechanism.

A user can create different topics. A topic entity is defined by its user and its name. A user can only manage its own topics, and can only associate them with buckets it owns.

In order to send notifications for events for a specific bucket, a notification entity needs to be created. A notification can be created on a subset of event types, or for all event types (default). The notification may also filter out events based on prefix/suffix and/or regular expression matching of the keys. As well as, on the metadata attributes attached to the object, or the object tags. There can be multiple notifications for any specific topic, and the same topic could be used for multiple notifications.

REST API has been defined to provide configuration and control interfaces for the bucket notification mechanism. This API is similar to the one defined as the S3-compatible API of the pubsub sync module.